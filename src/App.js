
import Cards from './components/Cards/Cards';
import Chart from './components/Chart/Chart';
import Country from './components/Country/Country';
import User from './components/User/User';
import styles from './App.module.css'
import {dataCovid, dataUser} from './api/';
import React from 'react';
class App extends React.Component {
  // constructor object, pass the data to props
  state = {
    data : {},
    data1 : {},
    
  }
  async componentDidMount() {
    const dataApi = await dataCovid();
    const userseed = await dataUser();
    this.setState({data:userseed})
    this.setState({data1:dataApi})
    
  }
  
  render() {
    return (
      <div className={styles.container}>
        <Cards data={this.state.data1}/>
        <User data={this.state.data}/>
          <Country />
          <Chart />
          
      </div>
    );
  }

  }



export default App;
