import axios from 'axios';

const url = 'https://covid19.mathdro.id/api';

export const dataCovid = async () => {
    try {
        const {data:{confirmed,recovered,deaths,lastUpdate}} = await axios.get(url);
        const apiUpdateData = {
            confirmed,
            recovered,
            deaths,
            lastUpdate,
        }
        return apiUpdateData
    } catch (error) {
        alert('Something went wrong with the API!');
    }
}


const url1 = 'https://randomuser.me/api';

export const dataUser = async () => {
    try {
        const {data:{info,results}} = await axios.get(url1);
        const apiUpdateData = {
            info,
            results,
        }
        return apiUpdateData
    } catch (error) {
        alert('Something went wrong with the API11!');
    }
}
