import React from 'react'
import {Card, CardContent, Typography, Grid} from '@material-ui/core';


function Cards({data :{confirmed,recovered,deaths,lastUpdate}}) {
    if(!confirmed) {
        return 'Loading...';
    }
    console.log(confirmed);
    
    return (
        <div>
            <div align="center">
                World Covid Data
            </div>
            <Grid container space={5} justify="center">
                <Grid item component={Card} >
                    <CardContent space={5}>
                        <Typography gutterBottom>Infected</Typography>
                        <Typography>{confirmed.value}</Typography>
                        <Typography>{new Date(lastUpdate).toDateString()}</Typography>
                        <Typography>Active Cases as of Today</Typography>
                    </CardContent>
                </Grid>
                <Grid item component={Card}>
                    <CardContent space={5}>
                        <Typography gutterBottom>Recovered</Typography>
                        <Typography>{recovered.value}</Typography>
                        <Typography>{new Date(lastUpdate).toDateString()}</Typography>
                        <Typography>Recoveres</Typography>
                    </CardContent>
                </Grid>
                <Grid item component={Card}>
                    <CardContent space={5}>
                        <Typography gutterBottom>Deaths</Typography>
                        <Typography>{deaths.value}</Typography>
                        <Typography>{new Date(lastUpdate).toDateString()}</Typography>
                        <Typography>Number of Deaths</Typography>
                    </CardContent>
                </Grid>
            </Grid>            
        </div>
    )
}

export default Cards
