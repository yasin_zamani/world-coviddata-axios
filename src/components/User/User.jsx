import React from 'react'
import {Card, CardContent, Typography, Grid, sizing} from '@material-ui/core';
import Image from 'material-ui-image'


function User({data :{info, results}}) {
    if(!info) {
        return 'Loading...';
    }
    
    
    console.log(results[0]);
    return (
        <div align="center">
            <p>Usage of AXIOS to fetch API data</p>
            <a href="https://randomuser.me/api">Random User info</a>
            <br></br>
            <a href="https://covid19.mathdro.id/api">Covid Data</a>
            
            <Grid container space={2} justify="center">
                <Grid item component={Card} >
                    <CardContent space={5}>
                        <Typography gutterBottom>API User Fetch</Typography>
                        <Typography gutterBottom>Name:{results[0].name.first} {results[0].name.last}</Typography>
                        <Typography gutterBottom>Age:{results[0].dob.age} {new Date(results[0].dob.date).toDateString()}</Typography>
                        <Typography gutterBottom>Gender:{results[0].gender}</Typography>
                        <Typography gutterBottom>Email:{results[0].email}</Typography>
                        <Image
  src={results[0].picture.large} 
/>
                    </CardContent>
                </Grid>            
            </Grid>            
        </div>
    )
}

export default User
